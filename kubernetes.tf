terraform {
  backend "s3" {
    bucket = "fashionunited.terraform"
    region = "eu-west-1"
    key    = "kubernetesdev"
  }
}

provider "aws" {
  region = "eu-west-1"
}

resource "aws_autoscaling_group" "master-eu-west-1c-masters-euwest1-develop-fashionunited-com" {
  name = "master-eu-west-1c.masters.euwest1.develop.fashionunited.com"
  launch_configuration = "${aws_launch_configuration.master-eu-west-1c-masters-euwest1-develop-fashionunited-com.id}"
  max_size = 1
  min_size = 1
  vpc_zone_identifier = ["${aws_subnet.eu-west-1c-euwest1-develop-fashionunited-com.id}"]
  tag = {
    key = "KubernetesCluster"
    value = "euwest1.develop.fashionunited.com"
    propagate_at_launch = true
  }
  tag = {
    key = "Name"
    value = "master-eu-west-1c.masters.euwest1.develop.fashionunited.com"
    propagate_at_launch = true
  }
  tag = {
    key = "k8s.io/role/master"
    value = "1"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_group" "nodes-euwest1-develop-fashionunited-com" {
  name = "nodes.euwest1.develop.fashionunited.com"
  launch_configuration = "${aws_launch_configuration.nodes-euwest1-develop-fashionunited-com.id}"
  max_size = 2
  min_size = 2
  vpc_zone_identifier = ["${aws_subnet.eu-west-1c-euwest1-develop-fashionunited-com.id}"]
  tag = {
    key = "KubernetesCluster"
    value = "euwest1.develop.fashionunited.com"
    propagate_at_launch = true
  }
  tag = {
    key = "Name"
    value = "nodes.euwest1.develop.fashionunited.com"
    propagate_at_launch = true
  }
  tag = {
    key = "k8s.io/role/node"
    value = "1"
    propagate_at_launch = true
  }
}

resource "aws_ebs_volume" "eu-west-1c-etcd-events-euwest1-develop-fashionunited-com" {
  availability_zone = "eu-west-1c"
  size = 20
  type = "gp2"
  encrypted = false
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "eu-west-1c.etcd-events.euwest1.develop.fashionunited.com"
    "k8s.io/etcd/events" = "eu-west-1c/eu-west-1c"
    "k8s.io/role/master" = "1"
  }
}

resource "aws_ebs_volume" "eu-west-1c-etcd-main-euwest1-develop-fashionunited-com" {
  availability_zone = "eu-west-1c"
  size = 20
  type = "gp2"
  encrypted = false
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "eu-west-1c.etcd-main.euwest1.develop.fashionunited.com"
    "k8s.io/etcd/main" = "eu-west-1c/eu-west-1c"
    "k8s.io/role/master" = "1"
  }
}

resource "aws_iam_instance_profile" "masters-euwest1-develop-fashionunited-com" {
  name = "masters.euwest1.develop.fashionunited.com"
  roles = ["${aws_iam_role.masters-euwest1-develop-fashionunited-com.name}"]
}

resource "aws_iam_instance_profile" "nodes-euwest1-develop-fashionunited-com" {
  name = "nodes.euwest1.develop.fashionunited.com"
  roles = ["${aws_iam_role.nodes-euwest1-develop-fashionunited-com.name}"]
}

resource "aws_iam_role" "masters-euwest1-develop-fashionunited-com" {
  name = "masters.euwest1.develop.fashionunited.com"
  assume_role_policy = "${file("data/aws_iam_role_masters.euwest1.develop.fashionunited.com_policy")}"
}

resource "aws_iam_role" "nodes-euwest1-develop-fashionunited-com" {
  name = "nodes.euwest1.develop.fashionunited.com"
  assume_role_policy = "${file("data/aws_iam_role_nodes.euwest1.develop.fashionunited.com_policy")}"
}

resource "aws_iam_role_policy" "masters-euwest1-develop-fashionunited-com" {
  name = "masters.euwest1.develop.fashionunited.com"
  role = "${aws_iam_role.masters-euwest1-develop-fashionunited-com.name}"
  policy = "${file("data/aws_iam_role_policy_masters.euwest1.develop.fashionunited.com_policy")}"
}

resource "aws_iam_role_policy" "nodes-euwest1-develop-fashionunited-com" {
  name = "nodes.euwest1.develop.fashionunited.com"
  role = "${aws_iam_role.nodes-euwest1-develop-fashionunited-com.name}"
  policy = "${file("data/aws_iam_role_policy_nodes.euwest1.develop.fashionunited.com_policy")}"
}

resource "aws_internet_gateway" "euwest1-develop-fashionunited-com" {
  vpc_id = "${aws_vpc.euwest1-develop-fashionunited-com.id}"
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "euwest1.develop.fashionunited.com"
  }
}

resource "aws_key_pair" "kubernetes-euwest1-develop-fashionunited-com-c6473032ccd2eca6ddfe55aefe9ef9c9" {
  key_name = "kubernetes.euwest1.develop.fashionunited.com-c6:47:30:32:cc:d2:ec:a6:dd:fe:55:ae:fe:9e:f9:c9"
  public_key = "${file("data/aws_key_pair_kubernetes.euwest1.develop.fashionunited.com-c6473032ccd2eca6ddfe55aefe9ef9c9_public_key")}"
}

resource "aws_launch_configuration" "master-eu-west-1c-masters-euwest1-develop-fashionunited-com" {
  name_prefix = "master-eu-west-1c.masters.euwest1.develop.fashionunited.com-"
  image_id = "ami-7e7f310d"
  instance_type = "m3.medium"
  key_name = "${aws_key_pair.kubernetes-euwest1-develop-fashionunited-com-c6473032ccd2eca6ddfe55aefe9ef9c9.id}"
  iam_instance_profile = "${aws_iam_instance_profile.masters-euwest1-develop-fashionunited-com.id}"
  security_groups = ["${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"]
  associate_public_ip_address = true
  user_data = "${file("data/aws_launch_configuration_master-eu-west-1c.masters.euwest1.develop.fashionunited.com_user_data")}"
  root_block_device = {
    volume_type = "gp2"
    volume_size = 20
    delete_on_termination = true
  }
  ephemeral_block_device = {
    device_name = "/dev/sdc"
    virtual_name = "ephemeral0"
  }
  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "nodes-euwest1-develop-fashionunited-com" {
  name_prefix = "nodes.euwest1.develop.fashionunited.com-"
  image_id = "ami-7e7f310d"
  instance_type = "t2.medium"
  key_name = "${aws_key_pair.kubernetes-euwest1-develop-fashionunited-com-c6473032ccd2eca6ddfe55aefe9ef9c9.id}"
  iam_instance_profile = "${aws_iam_instance_profile.nodes-euwest1-develop-fashionunited-com.id}"
  security_groups = ["${aws_security_group.nodes-euwest1-develop-fashionunited-com.id}"]
  associate_public_ip_address = true
  user_data = "${file("data/aws_launch_configuration_nodes.euwest1.develop.fashionunited.com_user_data")}"
  root_block_device = {
    volume_type = "gp2"
    volume_size = 20
    delete_on_termination = true
  }
  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_route" "0-0-0-0--0" {
  route_table_id = "${aws_route_table.euwest1-develop-fashionunited-com.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.euwest1-develop-fashionunited-com.id}"
}

resource "aws_route_table" "euwest1-develop-fashionunited-com" {
  vpc_id = "${aws_vpc.euwest1-develop-fashionunited-com.id}"
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "euwest1.develop.fashionunited.com"
  }
}

resource "aws_route_table_association" "eu-west-1c-euwest1-develop-fashionunited-com" {
  subnet_id = "${aws_subnet.eu-west-1c-euwest1-develop-fashionunited-com.id}"
  route_table_id = "${aws_route_table.euwest1-develop-fashionunited-com.id}"
}

resource "aws_security_group" "masters-euwest1-develop-fashionunited-com" {
  name = "masters.euwest1.develop.fashionunited.com"
  vpc_id = "${aws_vpc.euwest1-develop-fashionunited-com.id}"
  description = "Security group for masters"
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "masters.euwest1.develop.fashionunited.com"
  }
}

resource "aws_security_group" "nodes-euwest1-develop-fashionunited-com" {
  name = "nodes.euwest1.develop.fashionunited.com"
  vpc_id = "${aws_vpc.euwest1-develop-fashionunited-com.id}"
  description = "Security group for nodes"
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "nodes.euwest1.develop.fashionunited.com"
  }
}

resource "aws_security_group_rule" "all-master-to-master" {
  type = "ingress"
  security_group_id = "${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"
  source_security_group_id = "${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"
  from_port = 0
  to_port = 0
  protocol = "-1"
}

resource "aws_security_group_rule" "all-master-to-node" {
  type = "ingress"
  security_group_id = "${aws_security_group.nodes-euwest1-develop-fashionunited-com.id}"
  source_security_group_id = "${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"
  from_port = 0
  to_port = 0
  protocol = "-1"
}

resource "aws_security_group_rule" "all-node-to-master" {
  type = "ingress"
  security_group_id = "${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"
  source_security_group_id = "${aws_security_group.nodes-euwest1-develop-fashionunited-com.id}"
  from_port = 0
  to_port = 0
  protocol = "-1"
}

resource "aws_security_group_rule" "all-node-to-node" {
  type = "ingress"
  security_group_id = "${aws_security_group.nodes-euwest1-develop-fashionunited-com.id}"
  source_security_group_id = "${aws_security_group.nodes-euwest1-develop-fashionunited-com.id}"
  from_port = 0
  to_port = 0
  protocol = "-1"
}

resource "aws_security_group_rule" "https-external-to-master" {
  type = "ingress"
  security_group_id = "${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "master-egress" {
  type = "egress"
  security_group_id = "${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-egress" {
  type = "egress"
  security_group_id = "${aws_security_group.nodes-euwest1-develop-fashionunited-com.id}"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh-external-to-master" {
  type = "ingress"
  security_group_id = "${aws_security_group.masters-euwest1-develop-fashionunited-com.id}"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh-external-to-node" {
  type = "ingress"
  security_group_id = "${aws_security_group.nodes-euwest1-develop-fashionunited-com.id}"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_subnet" "eu-west-1c-euwest1-develop-fashionunited-com" {
  vpc_id = "${aws_vpc.euwest1-develop-fashionunited-com.id}"
  cidr_block = "172.20.96.0/19"
  availability_zone = "eu-west-1c"
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "eu-west-1c.euwest1.develop.fashionunited.com"
  }
}

resource "aws_vpc" "euwest1-develop-fashionunited-com" {
  cidr_block = "172.20.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "euwest1.develop.fashionunited.com"
  }
}

resource "aws_vpc_dhcp_options" "euwest1-develop-fashionunited-com" {
  domain_name = "eu-west-1.compute.internal"
  domain_name_servers = ["AmazonProvidedDNS"]
  tags = {
    KubernetesCluster = "euwest1.develop.fashionunited.com"
    Name = "euwest1.develop.fashionunited.com"
  }
}

resource "aws_vpc_dhcp_options_association" "euwest1-develop-fashionunited-com" {
  vpc_id = "${aws_vpc.euwest1-develop-fashionunited-com.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.euwest1-develop-fashionunited-com.id}"
}